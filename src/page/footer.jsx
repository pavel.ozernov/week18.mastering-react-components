import React from 'react';

function Footer({title = 'Footer Title'}) {
    return <h1 className="title">{title}</h1>
};

export default Footer;