import React, { Fragment } from "react";
import Photos from "./photos";



function Main(props) {
  
  return (
    <main>
      <Fragment>
        <h1 className="main-heading">Welcome to my photo album</h1>
        {props.children}
      </Fragment>
      <Photos photosData={props.photosData} />
    </main>
  );
}



export default Main;