import React from 'react';
import '../App.css';

function Header({title = 'Header Title'}) {
    return <h1 className="title">{title}</h1>
};

export default Header;