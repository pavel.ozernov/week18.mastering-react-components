import React, { Fragment } from "react";
import PropTypes from 'prop-types';

function Photos({ photosData }) {
  const firstTenPhotos = photosData.slice(0, 10);

  return (
    <Fragment>
    <h2 className="photos-heading">My Photos</h2>
    <div className="photos-container">
      {firstTenPhotos.map((photo) => (
        <div key={photo.id} className="photo">
          <h3>{photo.title}</h3>
          <img src={photo.url} alt={photo.title} />
        </div>
      ))}
    </div>
  </Fragment>
  );
}

Photos.propTypes = {
  photosData: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      imageUrl: PropTypes.string,
    }).isRequired
  ).isRequired,
};

export default Photos;